// @flow

/**
 * This script loads a fake DOM into scope so that tests using Enzyme's mount() API will
 * run outside of a browser.
 *
 * It is required to run *before* React is loaded, and is therefore handled via Mocha's --require parameter
 *
 * Cribbed from (and more info) here: https://github.com/airbnb/enzyme/blob/master/docs/guides/jsdom.md
 */

import { JSDOM } from 'jsdom';
const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
const { window } = jsdom;

function copyProps(src: any, target: any) {
  const props = Object.getOwnPropertyNames(src)
    .filter(prop => typeof target[prop] === 'undefined')
    .map(prop => Object.getOwnPropertyDescriptor(src, prop));
  Object.defineProperties(target, props);
}

global.window = window;
global.document = window.document;
global.navigator = {
  userAgent: 'node.js'
};

copyProps(window, global);
