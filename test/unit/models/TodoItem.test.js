import { expect } from 'chai';

import TodoItem from '../../../src/js/models/TodoItem';

describe('TodoItem', () => {
  describe('constructor', () => {
    it('creates an instance', () => {
      const target = new TodoItem();

      expect(target).to.exist;
    });

    it('initialises as incomplete', () => {
      const target = new TodoItem();

      expect(target.completed).to.be.false;
    });
  });
});
