import { expect } from 'chai';

import TodoItemStore from '../../../src/js/stores/TodoItemStore';
import TodoItem from '../../../src/js/models/TodoItem';
import { addItem, deleteItem, setItemComplete } from '../../../src/js/actions/todoActions';

describe('TodoItemStore', () => {
  describe('constructor', () => {
    it('creates an instance', () => {
      const target = new TodoItemStore();

      expect(target).to.exist;
    });

    it('begins with an empty list of items', () => {
      const store = new TodoItemStore();
      const target = store.state.items;

      expect(target).to.be.an('array').that.has.length(0);
    });
  });

  describe('addItem action', () => {
    it('should add an item to an empty store', () => {
      const store = new TodoItemStore();
      const expected = new TodoItem();

      addItem(expected);

      const items = store.state.items;

      expect(items).to.have.length(1);
      expect(items[0]).to.equal(expected);
    });

    it('should ignore an item if added twice in a row', () => {
      const store = new TodoItemStore();
      const expected = new TodoItem();

      addItem(expected);
      addItem(expected);

      const items = store.state.items;

      expect(items).to.have.length(1);
    });

    it('should add a second item', () => {
      const store = new TodoItemStore();
      const expected = new TodoItem();

      addItem(new TodoItem());
      addItem(expected);

      const items = store.state.items;

      expect(items).to.have.length(2);
      expect(items[1]).to.equal(expected);
    });

    it('should ignore an item if added twice', () => {
      const store = new TodoItemStore();
      const expected = new TodoItem();

      addItem(expected);
      addItem(new TodoItem());
      addItem(expected);

      const items = store.state.items;

      expect(items).to.have.length(2);
    });
  });

  describe('deleteItem action', () => {
    it('does nothing against an empty store', () => {
      const store = new TodoItemStore();

      deleteItem(new TodoItem());

      // mostly, we expect this test to simply Not Crash, but we'll check this just in case:
      expect(store.state.items).to.have.length(0);
    });

    it('does nothing when the item is not in the store', () => {
      const store = new TodoItemStore();

      addItem(new TodoItem());
      deleteItem(new TodoItem());

      expect(store.state.items).to.have.length(1);
    });

    it('removes a matching object from the store', () => {
      const store = new TodoItemStore();
      const item = new TodoItem();

      addItem(item);
      deleteItem(item);

      expect(store.state.items).to.have.length(0);
    });

    it('removes only the matching object from the store', () => {
      const store = new TodoItemStore();
      const items = store.state.items;
      const removedItem = new TodoItem();
      const remainingItem = new TodoItem();

      addItem(removedItem);
      addItem(remainingItem);
      deleteItem(removedItem);

      expect(items).to.have.length(1);
      expect(items[0]).to.equal(remainingItem);
    });
  });

  describe('setItemComplete action', () => {
    it('does nothing against an empty store', () => {
      const store = new TodoItemStore();

      setItemComplete(new TodoItem(), false);

      // mostly, we expect this test to simply Not Crash, but we'll check this just in case:
      expect(store.state.items).to.have.length(0);
    });

    it('does nothing when the item is not in the store', () => {
      const store = new TodoItemStore();

      addItem(new TodoItem());
      setItemComplete(new TodoItem(), true);

      expect(store.state.items[0].completed).to.be.false;
    });

    it('can complete an item', () => {
      const store = new TodoItemStore();
      const item = new TodoItem();

      addItem(item);
      setItemComplete(item, true);

      expect(store.state.items[0].completed).to.be.true;
    });

    it('completes only the passed item', () => {
      const store = new TodoItemStore();
      const otherItem = new TodoItem();
      const item = new TodoItem();

      addItem(otherItem);
      addItem(item);
      setItemComplete(item, true);

      expect(store.state.items[0].completed).to.be.false;
      expect(store.state.items[1].completed).to.be.true;
    });

    it('can uncomplete an item', () => {
      const store = new TodoItemStore();
      const item = new TodoItem();

      item.completed = true;

      addItem(item);
      setItemComplete(item, false);

      expect(store.state.items[0].completed).to.be.false;
    });

    it('uncompletes only the passed item', () => {
      const store = new TodoItemStore();
      const otherItem = new TodoItem();
      const item = new TodoItem();

      otherItem.completed = true;
      item.completed = true;

      addItem(otherItem);
      addItem(item);
      setItemComplete(item, false);

      expect(store.state.items[0].completed).to.be.true;
      expect(store.state.items[1].completed).to.be.false;
    });
  });
});
