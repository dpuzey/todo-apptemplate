import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import { shallow, mount } from 'enzyme';
import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import sinon from 'sinon';

chai.use(chaiEnzyme());

import ItemView from '../../../src/js/components/ItemView';
import TodoItem from '../../../src/js/models/TodoItem';
import { deleteItem, setItemComplete } from '../../../src/js/actions/todoActions';

const context = { router: { isActive: () => true, history: {} } };

describe('<ItemView />', () => {
  it('renders something', () => {
    const item = new TodoItem();
    const wrapper = shallow(<ItemView item={item} />, context);

    expect(wrapper).to.exist;
  });

  describe('delete item button', () => {
    it('renders a button to delete the item', () => {
      const item = new TodoItem();
      const wrapper = mount(<MemoryRouter><ItemView item={item} /></MemoryRouter>, context);
      const result = wrapper.find('.item-delete');

      expect(result).to.exist;
      expect(result).to.have.tagName('button');
    });

    it('invokes the deleteItem action and passes the item', () => {
      const spy = sinon.spy();

      const item = new TodoItem();
      const wrapper = mount(<MemoryRouter><ItemView item={item} /></MemoryRouter>, context);
      const button = wrapper.find('.item-delete');

      deleteItem.listen(spy);
      button.simulate('click');

      expect(spy.calledWith(item)).to.be.true;
    });
  });

  describe('complete item button', () => {
    it('renders a button to complete the item', () => {
      const item = new TodoItem();
      const wrapper = mount(<MemoryRouter><ItemView item={item} /></MemoryRouter>, context);
      const result = wrapper.find('.item-complete');

      expect(result).to.exist;
      expect(result).to.have.tagName('button');
    });

    it('invokes the completeItem action and passes the item', () => {
      const spy = sinon.spy();
      const item = new TodoItem();
      const wrapper = mount(<MemoryRouter><ItemView item={item} /></MemoryRouter>, context);
      const button = wrapper.find('.item-complete');

      setItemComplete.listen(spy);
      button.simulate('click');

      expect(spy.calledWith(item, true)).to.be.true;
    });

    it('sets completed to false when the item is already complete', () => {
      const spy = sinon.spy();
      const item = new TodoItem();

      item.completed = true;

      const wrapper = mount(<MemoryRouter><ItemView item={item} /></MemoryRouter>, context);
      const button = wrapper.find('.item-complete');

      setItemComplete.listen(spy);
      button.simulate('click');

      expect(spy.calledWith(item, false)).to.be.true;
    });
  });
});