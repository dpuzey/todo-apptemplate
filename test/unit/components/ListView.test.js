import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import { shallow, mount } from 'enzyme';
import React from 'react';
import { MemoryRouter } from 'react-router-dom';

chai.use(chaiEnzyme());

import ListView from '../../../src/js/components/ListView';

const context = { router: { isActive: () => true, history: {} } };

describe('<ListView />', () => {
  it('renders something', () => {
    const wrapper = shallow(<ListView />, context);

    expect(wrapper).to.exist;
  });

  describe('add-item link', () => {
    it('renders a link to add an item', () => {
      const wrapper = mount(<MemoryRouter><ListView /></MemoryRouter>, context);
      const result = wrapper.find('.item-add');

      expect(result).to.exist;
      expect(result).to.have.attr('href', '/create');
    });
  });
});
