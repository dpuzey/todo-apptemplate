# Todo list app

A Todo list app, created to demonstrate the basic architecture of a React app with Reflux and React-Router.

## What's in the box

- Babel for ES6 support
- Flow for type checking
- ESLint for style checking
- React for component structure and rendering
- Reflux for a lightweight single-data-flow structure
- React-router for navigation
- Basic npm build tasks

In particular: Reflux allows for the separation of components from the business logic of the app. The back-end of the app (the actions and stores) should be entirely unit-testable - which makes that critical part of the code fast and simple to test well. The components of the app should be easily testable using Enzyme as there is zero business logic within: all they do is render state and raise actions. The former can be modified easily for the test, and the latter can be mocked/stubbed to ensure the correct actions are raised.
