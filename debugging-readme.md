#Debugging Chrome with VSCode

## Ensure Chrome is running wirth a debug port

Edit your shortcut that runs Chrome, and add to the commandline `--remote-debugging-port=9222` in order to open Chrome to remote debugging.

## Add `launch.json`

This file describes the ways in which you can launch or attach to a debugging session. An example `launch.json` for Chrome debugging is below.

```
{
  "version": "0.2.0",
  "configurations": [
    {
      "type": "chrome",
      "request": "launch",
      "name": "Launch Chrome against localhost",
      "url": "http://localhost:8080",
      "webRoot": "${workspaceRoot}/build",
      "sourceMaps": true,
      "sourceMapPathOverrides": {
        "webpack:///./*": "${workspaceRoot}/src/*"
      },
      "runtimeExecutable": "C:\\Path\To\\chrome.exe --remote-debugging-port=9222",
    },
    {
      "type": "chrome",
      "request": "attach",
      "name": "Attach to Chrome",
      "port": 9222,
      "sourceMaps": true,
      "sourceMapPathOverrides": {
        "webpack:///./*": "${workspaceRoot}/src/*"
      },
      "webRoot": "${workspaceRoot}/build"
    }
  ]
}
```

The first important note here is that the content being served(by `webpack` or `http-server`) resides in the `/build` folder of the workspace. If that's not correct (e.g. if you're serving a `/www` folder instead), this path should be changed.

Secondly is the value for `sourceMapPathOverrides`, which is key to getting breakpoints & stepthrough working in built code. The value on the right will almost always be the same (it's the root of your source folder in the project). To find the value on the left (`webpack:///./*` in the example above), follow these steps:

1. Open your site in Chrome
1. Open the devtools and switch to the Sources tab
1. press Ctrl-P and enter the name of a js file (e.g. `CreateItem.js`)
1. Examine the dropdown for autocomplete. The webpack URL for the file will appear - for example, `webpack:///./js/components/CreateItem.js`
1. Strip the part of the path that is within your source folder (from `js` onward in this case)
1. The remainder of the path (`webpack:///./`) is the value for the left of the mapping (with an asterisk wildcard appended)
