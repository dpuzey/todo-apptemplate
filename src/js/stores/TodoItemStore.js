import Reflux from 'reflux';
import { addItem, deleteItem, setItemComplete } from '../actions/todoActions';
import TodoItem from '../models/TodoItem';

class TodoItemStore extends Reflux.Store {
  state: {
    items: Array<TodoItem>
  }

  constructor() {
    super();

    this.state = {
      items: []
    };

    this.listenTo(addItem, this.onAddItem);
    this.listenTo(deleteItem, this.onDeleteItem);
    this.listenTo(setItemComplete, this.onSetItemComplete);
  }

  onAddItem(item: TodoItem) {
    const { items } = this.state;

    if (items.indexOf(item) === -1) {
      items.push(item);

      this.setState({
        items: items
      });
    }
  }

  onDeleteItem(item: TodoItem) {
    const { items } = this.state;
    const index = items.indexOf(item);

    if (index > -1) {
      items.splice(index, 1);

      this.setState({
        items: items
      });
    }
  }

  onSetItemComplete(item: TodoItem, complete: bool) {
    const { items } = this.state;

    if (items.indexOf(item) > -1) {
      item.completed = complete;

      this.setState({
        items: items
      });
    }
  }
}

export default TodoItemStore;
