import React from 'react';
import Reflux from 'reflux';
import { Link, Redirect } from 'react-router-dom';
import TodoItemStore from '../stores/TodoItemStore';
import TodoItem from '../models/TodoItem';
import { deleteItem, setItemComplete } from '../actions/todoActions';

type Props = {};
type State = {
  deleted: bool
};

class DetailView extends Reflux.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      deleted: false
    };
    this.store = TodoItemStore;

    this.toggleComplete = this.toggleComplete.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
  }

  render(): React.Element {
    if (this.state.deleted) {
      return <Redirect to="/" />;
    }

    const item = this.getItem();
    const { title, description, created, completed } = item;

    return (<div id="content">
      <label>
        Title:
        <p>{title}</p>
      </label>
      <label>
        Description:
        <p>{description}</p>
      </label>
      <label>
        State:
        <p>{completed ? 'Complete' : 'Incomplete'}</p>
      </label>
      <label>
        Created:
        <p>{created.toString()}</p>
      </label>

      <button onClick={this.toggleComplete}>{completed ? 'Clear' : 'Complete'}</button>
      <button onClick={this.deleteItem}>Delete</button>
      <Link to="/">Back</Link>
    </div>);
  }

  toggleComplete() {
    const item = this.getItem();
    const newValue = !item.completed;

    setItemComplete(item, newValue);
  }

  deleteItem() {
    const item = this.getItem();

    deleteItem(item);

    this.setState({ deleted: true });
  }

  getItem(): TodoItem {
    const { match } = this.props;
    const { items } = this.state;
    const index = match.params.index;
    const item = items[index];

    return item;
  }
}

export default DetailView;
