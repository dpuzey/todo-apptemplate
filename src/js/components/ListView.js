import React from 'react';
import Reflux from 'reflux';
import { Link } from 'react-router-dom';
import TodoItemStore from '../stores/TodoItemStore';
import TodoItem from '../models/TodoItem';
import ItemView from './ItemView';

type Props = { };
type State = { };

class ListView extends Reflux.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {};
    this.store = TodoItemStore;
  }

  render(): React.Element<*> {
    const { items } = this.state;
    const listElements = items.map((v: TodoItem, i: number) => {
      return (<li key={i.toString()}>
        <ItemView item={v} index={i} />
      </li>);
    });

    return (<div id="content">
      list view
      <ul>
      { listElements }
      </ul>

      <Link to="/create" className="item-add">Add new</Link>
    </div>);
  }
}

export default ListView;
