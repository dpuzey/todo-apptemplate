import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import CreateItem from './CreateItem';
import Details from './Details';
import ListView from './ListView';

const App = () => (
  <Router>
    <div>
      <header>
          A Todo App
      </header>

      <Route exact path="/" component={ListView} />
      <Route path="/create" component={CreateItem} />
      <Route path="/details/:index" component={Details} />

      <footer>
        Created with fingers.
      </footer>
    </div>
  </Router>
);

export default App;
