import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import TodoItem from '../models/TodoItem';
import { addItem } from '../actions/todoActions';

type Props = { };
type State = {
  title: string,
  description: string,
  done: boolean
};

class CreateItem extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      title: '',
      description: '',
      done: false
    };

    this.createItem = this.createItem.bind(this);
  }

  render(): React.Element<*> {
    if (this.state.done) {
      return <Redirect to="/" />;
    }

    const { title, description } = this.state;

    return (<div>
      <label>
        Title:
        <input type="text" value={title} onChange={(e) => this.setState({ title: e.target.value })} />
      </label>
      <label>
        Description:
        <input type="text" value={description} onChange={(e) => this.setState({ description: e.target.value })} />
      </label>
      <button onClick={this.createItem}>Add</button>
      <Link to="/">Cancel</Link>
    </div>);
  }

  createItem() {
    const { title, description } = this.state;
    const item = new TodoItem();

    item.title = title;
    item.description = description;
    item.created = new Date();

    addItem(item);

    this.setState({ done: true });
  }
}

export default CreateItem;
