import React from 'react';
import { Link } from 'react-router-dom';
import TodoItem from '../models/TodoItem';
import { deleteItem, setItemComplete } from '../actions/todoActions';

type Props = {
  index: number,
  item: TodoItem
};

type State = { };

class ItemView extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {};

    this.toggleComplete = this.toggleComplete.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
  }

  render(): React.Element<*> {
    const { index, item } = this.props;
    const { title, completed } = item;

    return (<div>
      <Link to={`/details/${index}`}>{title}</Link>
      {completed ? <span className="item-completed">done</span> : undefined}
      <button onClick={this.toggleComplete} className="item-complete">{completed ? 'Clear' : 'Complete'}</button>
      <button onClick={this.deleteItem} className="item-delete">Delete</button>
    </div>);
  }

  toggleComplete() {
    const { item } = this.props;
    const newValue = !item.completed;

    setItemComplete(item, newValue);
  }

  deleteItem() {
    const { item } = this.props;

    deleteItem(item);
  }
}

export default ItemView;
