class TodoItem {
  title: string;
  description: string;
  created: Date;
  completed: bool;

  constructor() {
    this.completed = false;
  }
}

export default TodoItem;
