import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

const element = document.getElementById('appRoot');

ReactDOM.render(<App />, element);
