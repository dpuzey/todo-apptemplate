import Reflux from 'reflux';

export const addItem = Reflux.createAction();
export const deleteItem = Reflux.createAction();
export const setItemComplete = Reflux.createAction();
