module.exports = {
  env: {
    browser: true,
    es6: true,
    mocha: true
  },
  extends: ['eslint:recommended', 'plugin:react/recommended'],
  globals: {
    global: true
  },
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    }
  },
  plugins: [
    'react',
    'flowtype',
    'chai-expect'
  ],
  rules: {
    'comma-dangle': ['error', 'never'],
    'comma-spacing': ['error', { 'before': false, 'after': true }],
    'indent': ['error', 2],
    'no-multiple-empty-lines': 'error',
    'no-trailing-spaces': 'error',
    'padded-blocks': ['error', 'never'],
    'prefer-const': 'error',
    'prefer-template': 'error',
    'quotes': ['error', 'single', { avoidEscape: true }],
    'semi': ['error', 'always'],
    'flowtype/define-flow-type': 1,
    'flowtype/require-parameter-type': [1, { excludeArrowFunctions: true }],
    'flowtype/require-return-type': [1, 'always', { annotateUndefined: 'never', excludeArrowFunctions: true }],
    'flowtype/space-after-type-colon': [1, 'always'],
    'flowtype/space-before-type-colon': [1, 'never'],
    'flowtype/use-flow-type': 1,
    'flowtype/valid-syntax': 1,
    'chai-expect/missing-assertion': 1,
    'chai-expect/terminating-properties': 1,
    'chai-expect/no-inner-compare': 1
  },
  settings: {
    flowtype: {
      onlyFilesWithFlowAnnotation: false
    }
  }
};